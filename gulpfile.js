var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    rename      = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps');
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    jshint      = require('gulp-jshint'),
    scsslint    = require('gulp-sass-lint'),
    cache       = require('gulp-cached'),
    prefix      = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload,
    minifyHTML  = require('gulp-html-beautify');
    size        = require('gulp-size'),
    imgmin      = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    plumber     = require('gulp-plumber'),
    deploy      = require('gulp-gh-pages'),
    notify      = require('gulp-notify');
    jsprettify = require('gulp-jsbeautifier');
    removeHtmlComments = require('gulp-remove-html-comments');
    strip       = require('gulp-strip-comments');
    spritesmith = require('gulp.spritesmith');
    merge       = require('merge-stream');
    buffer      = require('vinyl-buffer');


gulp.task('scss', function() {
    var onError = function(err) {
      notify.onError({
          title:    "Gulp",
          subtitle: "Failure!",
          message:  "Error: <%= error.message %>",
          sound:    "Beep"
      })(err);
      this.emit('end');
  };

  return gulp.src('scss/*.scss')
    .pipe(plumber({errorHandler: onError}))
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: [
        'node_modules/bootstrap-sass/assets/stylesheets',
        'node_modules/slick-carousel',
        'node_modules/retinajs',
      ],
     }))
    .pipe(prefix())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/css'))
    .pipe(reload({stream:true})) 
}); 

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "dist/"
        }
    });
});

gulp.task('deploy', function () {
    return gulp.src('dist/**/*')
        .pipe(deploy());
});

gulp.task('fonts', function() {
    return gulp.src([
      'fonts/*'
    ])
    .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('js', function() { 
  gulp.src('js/*.js') 
    // .pipe(uglify())
    // .pipe(size({ gzip: true, showFiles: true }))
    .pipe(concat('functions.js'))
    .pipe(jsprettify({
      indent_size: 2
     }))
    .pipe(strip())
    .pipe(gulp.dest('dist/js'))
    .pipe(reload({stream:true}));
});

gulp.task('vendorjs', function() { 
  gulp.src([
    './node_modules/jquery/dist/jquery.js',
    './node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
    './node_modules/slick-carousel/slick/slick.js',
    './node_modules/retinajs/dist/retina.js',
    './node_modules/jquery-match-height/dist/jquery.matchHeight.js',
    ])
    .pipe(uglify())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist/js'))
    // .pipe(reload({stream:true}));
});

gulp.task('scss-lint', function() {
  gulp.src('scss/**/*.scss')
    .pipe(cache('scsslint'))
    .pipe(scsslint());
});

gulp.task('beautify-html', function() {
    var opts = {
      "indent_size": 2,
      "end_with_newline": false,
      "preserve_newlines": false,
    };

  gulp.src('./*.html')
    .pipe(removeHtmlComments())
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest('dist/'))
    .pipe(reload({stream:true}));
});

gulp.task('jshint', function() {
  gulp.src('js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('watch', function() {
  gulp.watch('scss/**/*.scss', ['scss']);
  gulp.watch('js/*.js', ['jshint', 'js']);
  gulp.watch('./*.html', ['beautify-html']);
  gulp.watch('images/*', ['imgmin']);
});

gulp.task('sprite', function () {
  var spriteData = gulp.src('images/sprite/*.png').pipe(spritesmith({
    retinaSrcFilter: 'images/sprite/*@2x.png',
    imgName: 'sprite.png',
    retinaImgName: 'sprite@2x.png',
    cssName: '_sprite.scss',
    algorithm: 'top-down',
    padding: 100,
  }));

  var imgStream = spriteData.img
    .pipe(buffer())
    .pipe(imgmin())
    .pipe(gulp.dest('images'));
 
  var cssStream = spriteData.css
    .pipe(gulp.dest('scss'));

  return merge(imgStream, cssStream);
});

gulp.task('imgmin', function () {
    return gulp.src('images/**/*.{jpg,png,gif,svg}')
        .pipe(imgmin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist/images'));
});

gulp.task('default', ['browser-sync', 'js', 'vendorjs', 'sprite', 'imgmin', 'beautify-html', 'scss', 'watch', 'fonts']);
