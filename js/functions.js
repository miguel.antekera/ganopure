$(document).ready(function() {

  $( '.close-bar' ).click(function() {
    $( '.top-bar' ).slideUp( '500', function() { });
    return false;
  });

  $('.testimonials-slide').slick({
      dots: false,
      infinite: true,
      autoplay: false,
      autoplaySpeed: 5000,
      speed: 1000,
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: false,
      responsive: [{
          breakpoint: 768,
          settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
          }
      }]
  });

  $('.pics').slick({
      dots: false,
      infinite: true,
      autoplay: false,
      autoplaySpeed: 5000,
      speed: 1000,
      slidesToShow: 5,
      slidesToScroll: 5,
      arrows: false,
      responsive: [{
          breakpoint: 768,
          settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
          }
      }]
  });

  $(function() {
      $('.box').matchHeight({
          byRow: true,
          property: 'height',
      });
  });

  // Ease scroll 
  $('.header a[href^="#"], .ease').on('click', function(e) {
      e.preventDefault();

      var target = this.hash;
      var $target = $(target);

      $('html, body').stop().animate({
          'scrollTop': $target.offset().top + 1
      }, 900, 'swing', function() {
          // window.location.hash = target;
      });

      $('.navbar-toggle').click();

      return false;
  });

  $(".trigger-modal").each(function(index) {
      $(this).on("click", function() {
          var contentName = $(this).attr("data-content");
          console.log(contentName);
          $('.modal-content').find('.' + contentName).show().siblings('.modal-body').hide();
      });
  });

});
